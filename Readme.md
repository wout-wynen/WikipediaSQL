### Deze repository bevat de SQL code die mijn UseCaseDiagramma moet voorstellen.
#### De code is getest met MYSQL Workbench. De code is bevat het maken van de database, het opstellen van tabellen en het opstellen van test query's.
#### Er is een markdown bestand waarin u de code visueel kan zien.
#### Als u de code wil testen, clone dan deze repository en gebruik het 'UseCaseWikipediaWoutWynen.sql' bestand in je MySQL Workbench.
